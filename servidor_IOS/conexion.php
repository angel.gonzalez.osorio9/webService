<html>

<head>
	<meta charset="utf-8" />
	<title></title>
</head>
<body>
<style type="text/css">
table th {background-color: yellow; }

table tr:nth-child(odd) {background-color: grey;}

table tr:nth-child(even) {background-color: pink;}
</style>

<?php
	// creación de la conexión a la base de datos con mysql_connect()
	$conexion = mysqli_connect('localhost', 'utp', 'utponiente') or die ("ERROR EN LA CONEXION");
	
	// Selección del a base de datos a utilizar
	$db = mysqli_select_db($conexion,'clientes') or die ( "NO HEMOS PODIDO CONECTAR A LA BASE DE DATOS" );

	// establecer y realizar consulta. guardamos en variable.
	$consulta = "SELECT * FROM cliente ";
	$resultado = mysqli_query( $conexion, $consulta ) or die ( "CONSULTA NO SE HA PODIDO REALIZAR CON EXITO");
	
	//CONTRUIMOS LA TABLA CON LOS TITULOS CORRESPONDIENTES
	
	echo "<table border='2'>";
	echo "<tr>";
	echo "<th>Id</th>";
	echo "<th>Nombre</th>";
	echo "<th>Apellido</th>";
	echo "<th>Edad</th>";
	echo "<th>Teléfono</th>";
	echo "<th>Estado civil</th>";
	echo "<th>Historial de compra</th>";
	echo "<th>Tipo de compra</th>";
	echo "<th>Ocupación</th>";
	echo "<th>Ubicación</th>";
	echo "<th>Código postal</th>";
	echo "<th>Sexo</th>";
	echo "</tr>";
	
	// recorre cada registro y muestra cada campo en la tabla.
	while ($columna = mysqli_fetch_array( $resultado ))
	{
		echo "<tr>";
		echo "<td>" . $columna['id'] . "</td>
		<td>" . $columna['nombre'] . "</td>
		<td>" . $columna['apellido'] . "</td>
		<td>" . $columna['edad'] . "</td>
		<td>" . $columna['telefono'] . "</td>
		<td>" . $columna['estadoCivil'] . "</td>
		<td>" . $columna['historialCompra'] . "</td>
		<td>" . $columna['tipoCompra'] . "</td>
		<td>" . $columna['ocupacion'] . "</td>
		<td>" . $columna['ubicacion'] . "</td>
		<td>" . $columna['codigoPostal'] . "</td>
		<td>" . $columna['sexo'] . "</td>";
		echo "</tr>";
	}
	
	echo "</table>"; // Fin de la tabla

	// cerrar conexión de base de datos
	mysqli_close( $conexion );
?>
</body>
</html>